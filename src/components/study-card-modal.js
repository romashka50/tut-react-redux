import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { editCard, setShowBack } from '../actions';

const mapStateToProps = ({ cards, showBack }, { params: { deckId } }) => ({
  deckId,
  showBack,
  card: cards.filter(card => card.deckId === deckId &&
  (!card.lastStudyOn || (new Date() - card.lastStudyOn) / 86400000 >= card.score))[0],
});
const mapDispatchToProps = dispatch => ({
  onFlip: () => dispatch(setShowBack(true)),
  onStudy: (cardId, score) => {
    const now = new Date();
    now.setHours(0, 0, 0, 0);

    dispatch(editCard({ id: cardId, score, lastStudyOn: +now }));
    dispatch(setShowBack());
  },
});
const StudyModal = ({ card, showBack, onFlip, onStudy, deckId }) => {
  let body = (<div className="no-card" >
    <p>You have no cards to study</p>
  </div>);

  if (card) {
    body = (<div className="study-card" >
      <div className={showBack ? 'front hide' : 'front'} >
        <div>
          <p>{card.front}</p>
        </div>
        <button onClick={onFlip} >Flip</button>
      </div>
      <div className={showBack ? 'back' : 'back hide'} >
        <div>
          <p>{card.back}</p>
        </div>
        <p>How did You do</p>
        <p>
          <button onClick={e => onStudy(card.id, Math.max(card.score - 1, 1))} >Poorly</button>
          <button onClick={e => onStudy(card.id, card.score)} >Good</button>
          <button onClick={e => onStudy(card.id, Math.min(card.score + 1, 3))} >Great</button>
        </p>
      </div>
    </div>);
  }

  return (<div className="modal study-modal" >
    <Link to={`/deck/${deckId}`} >x</Link>
    {body}
  </div>);
};

export default connect(mapStateToProps, mapDispatchToProps)(StudyModal);
