import React from 'react';
import { connect } from 'react-redux';
import SideBar from './sidebar';
import ToolBar from './toolbar';

const App = ({ deckId, children }) => (
  <div className="app">
    <ToolBar deckId={deckId} />
    <SideBar />
    <h1> Deck {deckId}</h1>
    {children}
  </div>
);

const mapStateToProps = (props, { params: { deckId } }) => ({
  deckId,
});

export default connect(mapStateToProps)(App);
