import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { addDeck, hideDeck } from '../actions';

const mapStateToProps = ({ decks, addingDeck }) => ({
  decks,
  addingDeck,
});
const mapDispatchToProps = dispatch => ({
  addDeck: name => dispatch(addDeck(name)),
  hideAddDeck: () => dispatch(hideDeck()),
});
const SideBar = React.createClass({
  componentDidUpdate() {
    const el = ReactDOM.findDOMNode(this.refs.add);
    return el && el.focus();
  },
  createDeck(e) {
    if (e.which !== 13) {
      return;
    }
    const name = ReactDOM.findDOMNode(this.refs.add).value;
    this.props.addDeck(name);
    this.props.hideAddDeck();
  },
  render() {
    const props = this.props;

    return (
      <div className="sidebar">
        <h2>All Decks</h2>
        <ul>
          {props.decks.map((deck, i) =>
            <li key={i}>
              <Link to={`/deck/${deck.id}`}>{deck.name}</Link>
            </li>,
          )}
        </ul>
        {props.addingDeck && <input ref="add" onKeyPress={this.createDeck} />}
      </div>
    );
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
