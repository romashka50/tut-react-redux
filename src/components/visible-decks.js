import React from 'react';
import { connect } from 'react-redux';
import fuzzysearch from 'fuzzysearch';
import Card from './card';

const match = (query, card) => fuzzysearch(query, card.front) || fuzzysearch(query, card.back);

const mapStateToProps = ({ cards, filterCards }, { params: { deckId } }) => ({
  cards: cards.filter(c => c.deckId === deckId && match(filterCards, c)),
});

const Cards = ({ cards, children }) => (
  <div className="main" >
    {cards.map(card => <Card card={card} key={card.id} />)}
    {children}
  </div>
);

export default connect(mapStateToProps)(Cards);
