import { connect } from 'react-redux';
import CardModal from './card-modal';
import { editCard, deleteCard } from '../actions';

const mapStateToProps = ({ cards }, { params: { cardId } }) => ({
  card: cards.filter(c => c.id === parseInt(cardId, 10))[0],
});

const mapDispatchToProps = dispatch => ({
  onSave: card => dispatch(editCard(card)),
  onDelete: cardId => dispatch(deleteCard(cardId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CardModal);
