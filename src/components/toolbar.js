import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { showDeck, filterCard } from '../actions';


const ToolBar = ({ deckId, showAddDeck, filterCard }) => {
  const deckTools = deckId ? (
      <div>
        <Link to={`/deck/${deckId}/new`}>Add new Card</Link>
        <Link to={`/deck/${deckId}/study`}>Study Deck</Link>
        <input
          className="search"
          type="search"
          placeholder="Search cards..."
          onChange={e => filterCard(e.target.value)}
        />
      </div>
    ) : null;

  return (
    <div>
      <div>
        <button onClick={showAddDeck}>Add new Deck</button>
      </div>
      {deckTools}
    </div>
  );
};

export default connect(null, {
  showAddDeck: showDeck,
  filterCard,
})(ToolBar);
