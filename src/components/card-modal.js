import React from 'react';
import { Link, browserHistory } from 'react-router';

class CardModal extends React.Component {
  constructor(props) {
    super(props);

    this.onSave = this.onSave.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentDidUpdate() {
    this.front.focus();
  }

  onSave(evt) {
    const front = this.front;
    const back = this.back;

    this.props.onSave(Object.assign({}, this.props.card, {
      front: front.value,
      back: back.value,
    }));

    browserHistory.push(`/deck/${this.props.card.deckId}`);
  }

  onDelete(evt) {
    this.props.onDelete(this.props.card.id);
    browserHistory.push(`/deck/${this.props.card.deckId}`);
  }

  render() {
    const { card, onDelete } = this.props;

    return (
      <div className="modal" >
        <h1> {onDelete ? 'Edit' : 'New'} Card</h1>
        <label htmlFor="front" >Card Front: </label>
        <textarea ref={(front) => {
          this.front = front;
        }} defaultValue={card.front} id="front"
        />
        <label htmlFor="back" >Card Back: </label>
        <textarea ref={(back) => {
          this.back = back;
        }} defaultValue={card.back} id="back"
        />
        <p>
          <button onClick={this.onSave} >Save Card</button>
          <Link className="btn" to={`/deck/${card.deckId}`} >Cancel</Link>
          {onDelete ?
            <button onClick={this.onDelete} >
              Delete Card
            </button> : null}
        </p>
      </div>);
  }
}

export default CardModal;
