import { connect } from 'react-redux';
import CardModal from './card-modal';
import { addCard } from '../actions';

const mapStateToProps = (state, { params : { deckId } }) => ({
  card: { deckId },
});

const mapDispatchToProps = dispatch => ({
  onSave: card => dispatch(addCard(card)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CardModal);
