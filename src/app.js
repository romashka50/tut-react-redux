import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import fetch from 'isomorphic-fetch';
import * as reducers from './reducers';
import App from './components/app';
import VisibleDecks from './components/visible-decks';
import NewCardModal from './components/new-card-modal';
import EditCardModal from './components/edit-card-modal';
import StudyCardModal from './components/study-card-modal';

import { fetchData } from './actions';

reducers.routing = routerReducer;

const store = createStore(combineReducers(reducers), applyMiddleware(thunk));
const history = syncHistoryWithStore(browserHistory, store);

const run = () => {
  console.log('--- call run ---');
  ReactDOM.render(
    <Provider store={store} >
      <Router history={history} >
        <Route path="/" component={App} >
          <Route path="/deck/:deckId" component={VisibleDecks} >
            <Route path="/deck/:deckId/new" components={NewCardModal} />
            <Route path="/deck/:deckId/edit/:cardId" components={EditCardModal} />
            <Route path="/deck/:deckId/study" components={StudyCardModal} />
          </Route>
        </Route>
      </Router>
    </Provider>, document.getElementById('root'));
};

function save() {
  const state = store.getState();

  fetch('/api/data', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      cards: state.cards,
      decks: state.decks,
    }),
  });
}

function init() {
  run();
  store.subscribe(run);
  store.subscribe(save);
  store.dispatch(fetchData());
}
init();
