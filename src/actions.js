import fetch from 'isomorphic-fetch';

export const addCard = card => ({ type: 'ADD_CARD', data: card });
export const editCard = card => ({ type: 'EDIT_CARD', data: card });
export const deleteCard = cardId => ({ type: 'DELETE_CARD', data: cardId });

export const filterCard = query => ({ type: 'FILTER_CARD', data: query });

export const addDeck = name => ({ type: 'ADD_DECK', data: name });
export const showDeck = () => ({ type: 'DECK:SHOW' });
export const hideDeck = () => ({ type: 'DECK:HIDE' });

export const setShowBack = show => ({
  type: 'SHOW_BACK',
  data: show,
});

export const receivedData = data => ({
  type: 'RECEIVED_DATA',
  data,
});

export const fetchData = () => (dispatch) => {
  fetch('/api/data')
    .then(response => response.json())
    .then(data => dispatch(receivedData(data)));
};
