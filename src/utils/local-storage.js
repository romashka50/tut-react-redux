export const get = item => JSON.parse(localStorage.getItem(item)) || undefined;
export const set = (state, props) => {
  const toSave = {};

  props.forEach(p => toSave[p] = state[p]);
  localStorage.setItem('state', JSON.stringify(toSave));
};
