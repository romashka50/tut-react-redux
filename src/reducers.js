export const cards = (state = [], action) => {
  switch (action.type) {
    case 'RECEIVED_DATA': {
      return action.data.cards || state;
    }
    case 'ADD_CARD': {
      const newCard = Object.assign({}, action.data, {
        score: 1,
        id: +new Date(),
      });

      return [...state, newCard];
    }
    case 'EDIT_CARD': {
      const card = action.data;

      return state.map(c => (card.id !== c.id ? c : Object.assign({}, c, card)));
    }
    case 'DELETE_CARD': {
      return state.filter(c => c.id !== action.data);
    }

    default:
      return state;
  }
};

export const decks = (state = [], action) => {
  switch (action.type) {
    case 'RECEIVED_DATA': {
      return action.data.decks || state;
    }
    case 'ADD_DECK': {
      const newDeck = {
        name: action.data,
        id: +new Date(),
      };

      return [...state, newDeck];
    }
    default:
      return state;
  }
};

export const addingDeck = (state = false, action) => {
  switch (action.type) {
    case 'DECK:SHOW':
      return true;
    case 'DECK:HIDE':
      return false;
    default:
      return !!state;
  }
};

export const filterCards = (state = '', action) => {
  console.log(state);
  switch (action.type) {
    case 'FILTER_CARD':
      return action.data;
    default:
      return state;
  }
};

export const showBack = (state = false, action) => {
  console.log(state);
  switch (action.type) {
    case 'SHOW_BACK':
      return !!action.data;
    default:
      return state;
  }
};

