const express = require('express');
const bodyParser = require('body-parser');

const app = express();

let data = {};

app.use(express.static(`${__dirname}/public`));
app.use(bodyParser.json());

app.get('/api/data', (req, res) => res.status(200).send(data));
app.post('/api/data', (req, res) => {
  data = req.body;
  res.status(200).send(data);
});
app.get('*', (req, res) => res.sendFile(__dirname + '/public/index.html'));

app.listen(3030, () => console.log('Server started success'));
